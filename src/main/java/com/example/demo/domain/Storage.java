package com.example.demo.domain;

import com.example.demo.Exception.NoSpaceException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {

  private Map<Ticket, Luggage> storageMap = new HashMap<>();

  private int capacity;

  //默认最大存8
  public Storage() {
    this(8);
  }

  public Storage(int capacity) {
    this.capacity = capacity;
  }

  public Ticket save(Luggage luggage) throws NoSpaceException {
    Ticket ticket = new Ticket();
    storageMap.put(ticket,luggage);
    if (storageMap.size() >= capacity) {
      throw new NoSpaceException("Sorry, there is no space.");
    }
    return ticket;
  }

  public Luggage getLuggage(Ticket ticket) {
    return storageMap.remove(ticket);
  }

  public Map<Ticket, Luggage> getStorageMap() {
    return storageMap;
  }

  public void setStorageMap(Map<Ticket, Luggage> storageMap) {
    this.storageMap = storageMap;
  }

  public int getCapacity() {
    return capacity;
  }
}
