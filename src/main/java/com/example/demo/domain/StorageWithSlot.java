package com.example.demo.domain;

import com.example.demo.Exception.NoSpaceException;
import com.example.demo.Exception.SlotSmallerThanLuggageException;
import com.example.demo.Exception.TicketIllegalException;

import java.util.*;
import java.util.stream.IntStream;

public class StorageWithSlot {
  private List<Slot> allSlots;
  private Map<Ticket,Slot> ticketSlotMap;

  public StorageWithSlot(int size) {
    initAllSlot(size);
    ticketSlotMap = new HashMap<>();
  }

  private void initAllSlot(int size) {
    allSlots = new ArrayList<>();
    IntStream.range(0,size).forEach(
      i ->{
        if (i<5){
          allSlots.add(new Slot(i,1,null));
        }
        else if (i<8) {
          allSlots.add(new Slot(i,2,null));
        }
        else {
          allSlots.add(new Slot(i,3,null));
        }
      }
    );
  }

  private Slot findAvailableSlot() {
    return this.allSlots.stream()
      .filter(Slot::isEmpty)
      .findFirst()
      .orElseThrow(RuntimeException::new);
  }

  private Ticket save(Luggage luggage, Slot slot) {
    slot.setLuggage(luggage);
    Ticket ticket = new Ticket();
    this.ticketSlotMap.put(ticket, slot);
    return ticket;
  }

  public Luggage get(Ticket ticket) {
    if (!ticketSlotMap.containsKey(ticket)) {
      throw new TicketIllegalException();
    }

    Slot slot = ticketSlotMap.remove(ticket);
    this.allSlots.remove(slot);
    return slot.getLuggage();
    /* 实例代码*/
//    return allSlots.stream()
//      .filter(slot -> Objects.equals(slot.getTicket(),ticket))
//      .findFirst()
//      .orElseThrow(()-> new RuntimeException("Invalid ticket!"))
//      .getLuggage();
  }

  public Ticket commonSave(Luggage luggage) {
    Slot slot = findAvailableSlot();
    return save(luggage, slot);
  }

  public Ticket saveLuggageInSpecificSlot(Integer num, Luggage luggage) {
    Slot slot = allSlots.get(num);

    if (ticketSlotMap.containsValue(slot)){
      throw new NoSpaceException("this slot is not available.");
    }
    else if(slot.getSize()<luggage.getSize()) {
      throw new SlotSmallerThanLuggageException();
    }
    else {
      return save(luggage, slot);
    }
  }

  public Map<Ticket, Slot> getTicketSlotMap() {
    return ticketSlotMap;
  }

  public List<Slot> getAllSlots() {
    return allSlots;
  }
}
