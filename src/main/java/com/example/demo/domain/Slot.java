package com.example.demo.domain;

import java.util.Map;
import java.util.Objects;

public class Slot {

  private Integer id;
  private Integer size;
  private Luggage luggage;


  public Slot(Integer id, Integer size, Luggage luggage) {
    this.id = id;
    this.size = size;
    this.luggage = luggage;
  }

  public Slot() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Luggage getLuggage() {
    return luggage;
  }

  public void setLuggage(Luggage luggage) {
    this.luggage = luggage;
  }

  public boolean isEmpty() {
    return Objects.equals(getLuggage(),null);
  }
}
