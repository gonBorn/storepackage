package com.example.demo.domain;

import java.util.List;

public class Luggage {
  private Integer size;

  public Luggage() {
  }

  public Luggage(Integer size) {
    this.size = size;
  }

  public Integer getSize() {
    return size;
  }
}
