package com.example.demo;

import com.example.demo.Exception.NoSpaceException;
import com.example.demo.domain.Assistant;
import com.example.demo.domain.Luggage;
import com.example.demo.domain.Ticket;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;


public class AssistantTest {


  @Test
  public void save_package_via_assistant_test() throws NoSpaceException {
    Luggage luggage = new Luggage();
    Assistant assistant = new Assistant();
    Ticket save = assistant.getStorage().save(luggage);
    assertNotNull(save);
  }

  @Test
  public void get_package_via_assistant_test() throws NoSpaceException {
    Luggage luggage = new Luggage();
    Assistant assistant = new Assistant();
    Ticket save = assistant.getStorage().save(luggage);
    assertEquals(luggage,assistant.getStorage().getLuggage(save));
  }

  @Test(expected = NoSpaceException.class)
  public void should_warn_if_there_is_no_capacity() throws NoSpaceException {
    //Storage mock = mock(Storage.class);
    //由于使用static无法使用mock
//    when(mock.save(new Luggage())).thenThrow(NoSpaceException.class);
    Assistant assistant = new Assistant();
    for (int i = 0; i <8 ; i++) {
      assistant.getStorage().save(new Luggage());

    }
    assistant.getStorage().save(new Luggage());
  }
}
