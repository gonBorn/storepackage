package com.example.demo;

import com.example.demo.Exception.NoSpaceException;
import com.example.demo.domain.Luggage;
import com.example.demo.domain.Storage;
import com.example.demo.domain.Ticket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void store_test() throws NoSpaceException {
		Storage storage = new Storage();
		assertNotNull(storage.save(new Luggage()));
	}

	@Test
	public void get_test() throws NoSpaceException {
		Storage storage = new Storage();
		Ticket save = storage.save(new Luggage());
		Luggage luggage = storage.getLuggage(save);
		assertNotNull(luggage);
		assertNull(storage.getStorageMap().get(save));
	}

	@Test
	public void save_null_get_null_test() throws NoSpaceException {
		Storage storage = new Storage();
		Ticket save = storage.save(null);
		assertNull(storage.getStorageMap().get(save));
	}

	@Test
	public void save_two_and_get_the_second_test() throws NoSpaceException {
		Luggage first = new Luggage();
		Luggage second = new Luggage();
		Storage storage = new Storage();
		Ticket save1 = storage.save(first);
		Ticket save2 = storage.save(second);
		assertEquals(second, storage.getLuggage(save2));
	}


	//junit4
	@Test(expected = NoSpaceException.class)
	public void should_warn_if_there_is_no_capacity() throws NoSpaceException {
		Storage storage = new Storage(0);
		storage.save(new Luggage());
	}

	//via assistant

}
