package com.example.demo;

import com.example.demo.Exception.NoSpaceException;
import com.example.demo.Exception.SlotSmallerThanLuggageException;
import com.example.demo.Exception.TicketIllegalException;
import com.example.demo.domain.Luggage;
import com.example.demo.domain.StorageWithSlot;
import com.example.demo.domain.Ticket;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class SlotTest {

  @Test
  public void should_common_save_success_if_slot_available_test() {
    Luggage luggage = new Luggage();
    StorageWithSlot storageWithSlot = new StorageWithSlot(8);
    Ticket ticket = storageWithSlot.commonSave(luggage);
    assertNotNull(ticket);
  }

  @Test
  public void should_success_if_bag_smaller_or_equal_slot_test() {
    Luggage luggage = new Luggage(1);
    StorageWithSlot storageWithSlot = new StorageWithSlot(8);
    Ticket ticket = storageWithSlot.saveLuggageInSpecificSlot(1, luggage);
    assertEquals(luggage,storageWithSlot.getTicketSlotMap().get(ticket).getLuggage());
  }

  @Test(expected = NoSpaceException.class)
  public void should_fail_if_specific_slot_is_not_available_test() {
    Luggage luggage = new Luggage(1);
    StorageWithSlot storageWithSlot = new StorageWithSlot(8);
    storageWithSlot.saveLuggageInSpecificSlot(1, luggage);
    storageWithSlot.saveLuggageInSpecificSlot(1, luggage);
  }

  @Test(expected = SlotSmallerThanLuggageException.class)
  public void should_fail_if_specific_slot_is_too_small_test() {
    Luggage luggage = new Luggage(3);
    StorageWithSlot storageWithSlot = new StorageWithSlot(8);
    storageWithSlot.saveLuggageInSpecificSlot(1, luggage);
  }

  @Test(expected = TicketIllegalException.class)
  public void should_throw_exception_if_ticket_is_illegal_test() {
    StorageWithSlot storageWithSlot = new StorageWithSlot(8);
    Luggage luggage = new Luggage();
    Ticket trueTicket = storageWithSlot.commonSave(luggage);
    assertEquals(luggage,storageWithSlot.get(trueTicket));
    storageWithSlot.get(new Ticket());
  }

  @Test
  public void should_return_null_if_luggage_was_taken_by_ticket_test() {
    StorageWithSlot storageWithSlot = new StorageWithSlot(8);

    Ticket trueTicket = storageWithSlot.saveLuggageInSpecificSlot(1,new Luggage(1));
    assertNotNull(storageWithSlot.getAllSlots().get(1).getLuggage());

    storageWithSlot.get(trueTicket);

    assertNull(storageWithSlot.getAllSlots().get(1).getLuggage());
  }
}
